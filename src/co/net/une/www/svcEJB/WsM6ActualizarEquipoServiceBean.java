package co.net.une.www.svcEJB;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Servicios WEB GSS SmallWorld 4.3
##	Archivo: WsM6ActualizarEquipoService.java
##	Contenido: Clase que contiene la implementaci�n del servicio WsM6ActualizarEquipoService
##	Autor: Freddy Molina
##  Fecha creaci�n: 19-02-2016
##	Fecha �ltima modificaci�n: 19-02-2016
##	Historial de cambios: 
##	19-02-2016	FJM		Primera versi�n
##
##**********************************************************************************************************************************
*/

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.ncainvm6.Atributotype;
import co.net.une.www.ncainvm6.BoundedString14;
import co.net.une.www.ncainvm6.Detallerespuestatype;
import co.net.une.www.ncainvm6.Listaatributostype;
import co.net.une.www.ncainvm6.Listapuertotype;
import co.net.une.www.ncainvm6.Puertostype;
import co.net.une.www.ncainvm6.UTCDate;
import co.net.une.www.ncainvm6.WsM6ActualizarEquipoRSType;

import com.gesmallworld.gss.lib.service.ServiceLocal;
import com.gesmallworld.gss.lib.service.magik.MagikService;
import com.gesmallworld.gss.lib.auth.AuthorisationException;
import com.gesmallworld.gss.lib.exception.GSSException;
import com.gesmallworld.gss.lib.locator.ServiceLocator;
import com.gesmallworld.gss.lib.locator.ServiceLocatorException;
import com.gesmallworld.gss.lib.log.SWLog;
import com.gesmallworld.gss.lib.request.BusinessResponse;
import com.gesmallworld.gss.lib.request.ParameterException;
import com.gesmallworld.gss.lib.request.Request;

import javax.ejb.SessionBean;

import org.apache.log4j.Level;

import com.gesmallworld.gss.lib.request.Response;
import com.gesmallworld.gss.lib.service.magik.MagikService.StateHandling;

/**
 * Clase que contiene la implementaci�n del servicio WsM6ActualizarEquipoService
 * @author FreddyMolina
 * @creado 19/02/2016
 * @ultimamodificacion 19/02/2016
 * @version 1.0
 * 
 * @ejb.resource-ref res-type="javax.resource.cci.ConnectionFactory"
 *                   res-auth="Container" res-ref-name="eis/SmallworldServer"
 *                   jndi-name="eis/SmallworldServer"
 *                   res-sharing-scope="Shareable"
 * @ejb.interface local-extends=
 *                "javax.ejb.EJBLocalObject, com.gesmallworld.gss.lib.service.ChainableServiceLocal"
 *                package="co.net.une.www.interfaces"
 * @ejb.bean view-type="local" name="WsM6ActualizarEquipoService"
 *           type="Stateless"
 *           local-jndi-name="ejb/WsM6ActualizarEquipoServiceLocal"
 *           transaction-type="Bean"
 * @ejb.home local-extends="javax.ejb.EJBLocalHome"
 *           package="co.net.une.www.interfaces"
 */
@StateHandling(serviceProvider = "ws_m6_actualizar_equipo_service_service_provider")
public class WsM6ActualizarEquipoServiceBean extends MagikService implements SessionBean {

	/**
	 * Local jndi name in use for this service bean.
	 */
	public static final String LOCAL_JNDI_NAME = "ejb/WsM6ActualizarEquipoServiceLocal";

	/**
	 * Serialisation ID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Mensajes en properties
	 */
	//archivo de propiedades
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.WsActualizarEquipos", Locale.getDefault());
	//validaci�n gen�rica de errores GSS
	private static final String KeyGSSException = "validate.GSSException.code";
	//validaci�n gen�rica de errores
	private static final String KeyGeneralException = "validate.generalException.code";
	//validaci�n de par�metros repetidos
    private static final String KeyRequiredParameterException = "validate.required.parameterException.code";
    //validaci�n tipo de equipo valido
    private static final String keyTipoEquipoException = "validate.tipoEquipo";
    //validaci�n nroPuertosDif valido
    private static final String keyPuertosDif = "validate.nroPuertosDif";
    //validaci�n nroPuertosDif valido
    private static final String keyRFSRep = "validate.RFSRep";
    //mensaje de error de par�metro inv�lido
	private String mensajeErrorParametroInvalido = "";
    

    //EJB encadenado
    private static final String KeyChainedEJB =  rb.getString("parameter.chainedEJB");
    //funci�n EJB a ejecutar
    private static final String KeyChainedEJBFunction = rb.getString("parameter.chainedEJB.function");
    //nombre del servicio
    private static final String keyNombreServicio = rb.getString("servicio.nombre");
    //nombre de la funci�n
    private static final String keyNombreFuncion = rb.getString("servicio.nombrefuncion");
    
	//validaci�n de nro puerto y �ltimo valor del RFS
    private static final String KeyNroPuertoRFS = "validate.NroPuertoRFS";
	//validaci�n de nro puerto y �ltimo valor del RFS
    private static final String KeyEstadoPuertoDir = "validate.estadoPuertoDir";
    //estado de puerto: Libre
    private static final String puertoLibre = rb.getString("puerto.estado.libre");
    //estado de puerto: Ocupado
    private static final String puertoOcupado = rb.getString("puerto.estado.ocupado");
    //estado de puerto: Bloqueado
    private static final String puertoBloqueado = rb.getString("puerto.estado.bloqueado");
    //tipo de equipo valido
    private static final String keyTipoEquipo = rb.getString("validate.tipoEquipoValido");

	/**
	 * Constructor.
	 */
	public WsM6ActualizarEquipoServiceBean() {
		super();
	}

	/**
	 * Generated service proxy method. Corresponds to a service of a Magik
	 * Service Provider.
	 * 
	 * @ejb.interface-method
	 * @param request
	 *            A request object as specified in the service description file.
	 * @return A response instance
	 */
	@SuppressWarnings("unchecked")
	@EISMapping(value = "actualizar_equipo")
	public Response actualizarEquipo(Request request) {
		
		//se configuran los par�metros de salida
		WsM6ActualizarEquipoRSType actualizaEquipoRSType = new WsM6ActualizarEquipoRSType();
	
		// inicializo los par�metros de respuesta
		inicializarParametrosSalida(actualizaEquipoRSType);
		
		// Defino puente entre request y response
		Response response = new BusinessResponse(request);
		
		try {
			
			//Obtengo los parametros de entrada del servicio
			//obtengo la fecha de la solicitud
			UTCDate fechaSolicitud = (UTCDate) request.getParameter("fechaSolicitud");
			//obtengo IdUneEquipo
			String idUneEquipo = (String) request.getParameter("idUneEquipo");
			//obtengo tipoEquipo
			String tipoEquipo = (String) request.getParameter("tipoEquipo");
			//obtengo cantidadPuertos
			String cantidadPuertos = (String) request.getParameter("cantidadPuertos");	
			//obtengo lista de atributos
			Listaatributostype listaAtributos = (Listaatributostype)  request.getParameter("atributos");
			//obtengo lista de puertos
			Listapuertotype listaPuertos = (Listapuertotype)  request.getParameter("puertos");
			
			
			//actualizo los par�metros de salida con valores de entrada
			//actualizo fecha de solicitud
			actualizaEquipoRSType.setFechaSolicitud(fechaSolicitud);
			//actualizo IdUneEquipo
			actualizaEquipoRSType.setIdUneEquipo(idUneEquipo);
			//actualizo TipoEquipo
			actualizaEquipoRSType.setTipoEquipo(tipoEquipo);
			//actualizo cantidad de puertos
			actualizaEquipoRSType.setCantidadPuertos(cantidadPuertos);
			//la fecha de solicitud de la salida debe ser la misma que la de la entrada
			actualizaEquipoRSType.setFechaSolicitud(fechaSolicitud);
			
			//valido los par�metros de entrada
			//FechaSolicitud es obligatorio
			validarCampoObligatorio("FechaSolicitud", fechaSolicitud, actualizaEquipoRSType.getDetalleRespuesta());	
			//IdUneEquipo es obligatorio
			validarCampoObligatorio("IdUneEquipo", idUneEquipo, actualizaEquipoRSType.getDetalleRespuesta());
			//TipoEquipo es obligatorio
			validarCampoObligatorio("TipoEquipo", tipoEquipo, actualizaEquipoRSType.getDetalleRespuesta());
			//valido que el tipo de equipo sea solo TAP
			if (!keyTipoEquipo.equals(tipoEquipo)){
				//el campo es diferente, lanzo excepcion
				registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(keyTipoEquipoException + ".code"), MessageFormat.format(rb.getString(keyTipoEquipoException), ""), null, false);
				lanzarParameterException();
			}
			//TipoEquipo es obligatorio
			validarCampoObligatorio("CantidadPuertos", cantidadPuertos, actualizaEquipoRSType.getDetalleRespuesta());
			//Puertos es obligatorio
			validarCampoObligatorio("Puertos", listaPuertos, actualizaEquipoRSType.getDetalleRespuesta());			
						
			//obtengo el listado de atributos si est�n presentes
			Listaatributostype listaAtribSalida =null;
			if (listaAtributos!=null && !"".equals(listaAtributos)) {
				//recorro los atributos
				listaAtribSalida = new Listaatributostype();
				//por cada atributo de la lista
				boolean ingrese=false;
				for(Atributotype atributo:listaAtributos.getAtributo()){
					if(!ServiciosUtil.esCampoVacio(atributo.getIdAtributo()) && !ServiciosUtil.esCampoVacio(atributo.getIdAtributoPadre())
							&& !ServiciosUtil.esCampoVacio(atributo.getNombreAtributo()) && !ServiciosUtil.esCampoVacio(atributo.getValorAtributo())){
						//Si todos los campos estan completos lo ingreso en la lista
						listaAtribSalida.addAtributo(atributo);
						ingrese= true;
					}
				}
				if (!ingrese)
					listaAtribSalida = null;
			}
			
			//se arma la entrada de los puertos para el magik
			ArrayList<HashMap<String, Object>> puertosList = new ArrayList<HashMap<String,Object>>();
			
			//varible para control de puertos
			int contPuertos = 0;
			ArrayList<String> RFSList = new ArrayList<String>();
			//recorro todos los elementos de la lista
			for(Puertostype puertotype:listaPuertos.getPuerto()){

				//realizo validaciones sobre los puertos
				//el n�mero de puerto debe ser igual al �ltimo caracter del valor del RFS
				if (!puertotype.getRFS().endsWith(puertotype.getNroPuerto())){
					//el n�mero de puerto es diferente al �ltimo caracter del valor del RFS, lanzo excepcion
					registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyNroPuertoRFS + ".code"), MessageFormat.format(rb.getString(KeyNroPuertoRFS), ""), null, false);
					lanzarParameterException();
				}
				
				//el estado si es libre debe tener idDireccion o si es estado es bloqueado o ocupado la idDireccion no debe ser vacio
				if ( (puertoLibre.equals(puertotype.getEstado()) && !"".equals(puertotype.getIdDireccion())) ||
					 ( (puertoOcupado.equals(puertotype.getEstado())|| puertoBloqueado.equals(puertotype.getEstado())) &&
					   "".equals(puertotype.getIdDireccion()))
				   ){
					//No cumple con la condici�n de el estado si es libre debe tener idDireccion o si es estado es bloqueado o ocupado la idDireccion no debe ser vacio
					registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyEstadoPuertoDir + ".code"), MessageFormat.format(rb.getString(KeyEstadoPuertoDir), ""), null, false);
					lanzarParameterException();
				}
				
				//valido que no exista un RFS reperido en la entrada
				if (RFSList.indexOf(puertotype.getRFS()) >=0) {
					//Ya existe un RFS repetido en la entrada, lanzo una excepcion
					registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(keyRFSRep + ".code"), MessageFormat.format(rb.getString(keyRFSRep), ""), null, false);
					lanzarParameterException();
				}
				
				//armo el objeto puerto
				HashMap<String, Object> puertoHash = new HashMap<String, Object>();			
				puertoHash.put("NroPuerto", puertotype.getNroPuerto());
				puertoHash.put("Estado", puertotype.getEstado());
				puertoHash.put("RFS", puertotype.getRFS());
				puertoHash.put("IdDireccion", puertotype.getIdDireccion());
				//ingreso el puerto
				puertosList.add(puertoHash);
				//incremento el contador
				contPuertos++;
				//ingreso el RFS a la lista de control
				RFSList.add(puertotype.getRFS());
			}
			
			//valido que la cantidad de puertos coincida con el par�metro CantidadPuertos
			if (Integer.parseInt(cantidadPuertos)!= contPuertos){
				//la cantidad de puertos no coincide con el conteo, lanzo excepci�n
				registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(keyPuertosDif + ".code"), MessageFormat.format(rb.getString(keyPuertosDif), ""), null, false);
				lanzarParameterException();
				
			}
			
			// Encadeno la llamada al Magik
			Request r = request.createChainedRequest(KeyChainedEJB, KeyChainedEJBFunction);
			
			// Asigno el parametro de entrada al servicio Magik
			HashMap<String, Object> parametros = new HashMap<String, Object>();
			//ingreso idUneEquipo
			parametros.put("idUneEquipo", idUneEquipo);
			//ingreso tipoEquipo
			parametros.put("tipoEquipo",tipoEquipo);
			//ingreso la lista de puertos
			parametros.put("puertos",puertosList);
			
			r.setParameter("parametros", parametros);
			
			//Ejecuto el servicio Magik
			ServiceLocal s = (ServiceLocal) ServiceLocator.getInstance().getSLSB(KeyChainedEJB);
			Response gssServiceResponse = s.makeRequest(r);

			//Obtengo la respuesta del servicio Magik
			Map<String, Object> respuestaMagik = gssServiceResponse.getResponses();
			HashMap<String, Object> respuesta = (HashMap<String, Object>) respuestaMagik.get("respuesta");
			
			//obtengo el detalle de la respuesta
			HashMap<String, Object> detalleRespuesta = (HashMap<String, Object>)respuesta.get("DetalleRespuesta") ;
			actualizaEquipoRSType.getDetalleRespuesta().setCodigoError(ServiciosUtil.depurarParametro(detalleRespuesta,"CodigoError"));
			actualizaEquipoRSType.getDetalleRespuesta().setCodigoRespuesta(ServiciosUtil.depurarParametro(detalleRespuesta,"CodigoRespuesta"));
			actualizaEquipoRSType.getDetalleRespuesta().setDescripcionError(ServiciosUtil.depurarParametro(detalleRespuesta,"DescripcionError"));
			actualizaEquipoRSType.getDetalleRespuesta().setMensajeError(ServiciosUtil.depurarParametro(detalleRespuesta,"MensajeError"));
			HashMap<String, GregorianCalendar> fechaSol = (HashMap<String, GregorianCalendar>) detalleRespuesta.get("FechaRespuesta");
			actualizaEquipoRSType.getDetalleRespuesta().setFechaRespuesta(obtenerFechaSolicitud((GregorianCalendar) fechaSol.get("date")));
			
			//obtengo el par�metro puertosActualizados
			actualizaEquipoRSType.setPuertosActualizados(ServiciosUtil.depurarParametro(respuesta, "puertosActualizados"));
			
			//el listado de atributos es el mismo de la entrada
			if (listaAtribSalida!=null)
				actualizaEquipoRSType.setAtributos(listaAtribSalida);
			
		}catch (ParameterException e) {
			System.out.println(mensajeErrorParametroInvalido);
		} catch (ServiceLocatorException e) {
			registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion), e, true);
		} catch (AuthorisationException e) {
			registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion), e, true);
		} catch (GSSException e) {
			registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGSSException), MessageFormat.format(ServiciosUtil.getMensajeErrorGSS(),keyNombreFuncion),  e, true);
		}catch (Exception e) {
			registrarException(actualizaEquipoRSType.getDetalleRespuesta(), rb.getString(KeyGeneralException), MessageFormat.format(ServiciosUtil.getMensajeErrorGeneral(),keyNombreFuncion), e, true);
		} finally {
			try {
				// Envia la respuesta al servicio
				response.addResponse("response", actualizaEquipoRSType);
			} catch (ParameterException e) {
				SWLog.log(Level.ERROR, this, e.getMessage());
			}
		}

		return response;
	}

	/**
	 * Funci�n que registra la excepci�n en los par�metros de salida
	 * @param generalType
	 * 			Objeto de respuesta del proceso del servicio
	 * @param codigoParam
	 * 			Mensaje del c�digo de error
	 * @param mensaje
	 * 			Mensaje del error
	 * @param e
	 * 			Excepcion a reportar
	 * @param mostrarTrace
	 * 			Verdadero para mostrar el traceRoot de la Excepcion, falso en cualquier otro caso
	 */
	private void registrarException(Detallerespuestatype generalType, String codigoParam, String mensaje, Exception e, boolean mostrarTrace){
		
		//Lleno los par�metros de respuesta
		generalType.setCodigoError(codigoParam);
		generalType.setDescripcionError(mensaje);
		generalType.setMensajeError(mensaje);
		
		//Si mostrarTrace es TRUE se muestra el traceRoot
		if (mostrarTrace) e.printStackTrace();
		
	}

	/**
	 * Funci�n que inicializa los par�metros de salida del servicio
	 * @param actualizaEquipoRSType
	 * 			Par�metros de salia a inicializar
	 */
	private void inicializarParametrosSalida(WsM6ActualizarEquipoRSType actualizaEquipoRSType) {
		//inicializo la cantidadPuertos
		actualizaEquipoRSType.setCantidadPuertos("");
		//inicializo idEquipoUNE
		actualizaEquipoRSType.setIdUneEquipo("");
		//inicializo los puertosActualizados
		actualizaEquipoRSType.setPuertosActualizados("NO");
		//inicializo los tipoEquipo
		actualizaEquipoRSType.setTipoEquipo(keyTipoEquipo);
		
		//creo el detalle de la respuesta
		Detallerespuestatype detalleRespuesta = new Detallerespuestatype();
		//inicializo c�digo de Error
		detalleRespuesta.setCodigoError("");
		//inicializo c�digo de respuesta, por defecto es falla
		detalleRespuesta.setCodigoRespuesta(ServiciosUtil.KO);
		//inicializo descripci�n del error
		detalleRespuesta.setDescripcionError("");
		//creo la fecha de respuesta, por defecto la fecha del sistema
		UTCDate utcDate = new UTCDate();
		//obtengo la fecha del sistema
		Date ahora = new Date();
		//le doy formato la fecha del sistema
		SimpleDateFormat format = new SimpleDateFormat(ServiciosUtil.getFormatoFechaDefault());
		BoundedString14 boundedString14 = new BoundedString14();
		boundedString14.setBoundedString14(format.format(ahora.getTime()));
		utcDate.setDate(boundedString14);
		//asigno la fecha
		detalleRespuesta.setFechaRespuesta(utcDate);
		//inicializo mensaje del error
		detalleRespuesta.setMensajeError("");
		//asigno la respuesta a la salida
		actualizaEquipoRSType.setDetalleRespuesta(detalleRespuesta);
		

		//inicializo la fecha de la solicitud, en caso que falte alg�n par�metro obligatorio
		actualizaEquipoRSType.setFechaSolicitud(utcDate);
	}
	
	/**
	 * Funci�n que valida si el campo es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param valorCampo
	 * 			Valor del campo a validar
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, String valorCampo,	Detallerespuestatype detalleRespuesta) throws ParameterException {
		// valido que el campo no sea vac�o
		if (ServiciosUtil.esCampoVacio(valorCampo)) {
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(detalleRespuesta, nombreCampo);
		}	
	}
	
	
	/**
	 * Funci�n que valida si el campo es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param valorCampo
	 * 			Valor del campo a validar
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, Listapuertotype listaPuertos, Detallerespuestatype detalleRespuesta) throws ParameterException {
		
		//valido que la lista de puertos no est� vac�o
		if (listaPuertos==null || listaPuertos.getPuerto().length ==0 ){
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(detalleRespuesta, nombreCampo);
		}
		
		//valido que no vengan un tipo Dummy
		if (listaPuertos.getPuerto().length ==1){
			//obtengo el primer elemento
			Puertostype puerto = listaPuertos.getPuerto()[0];
			//se valido que el puerto sea dummy
			if (ServiciosUtil.esCampoVacio(puerto.getEstado())&& ServiciosUtil.esCampoVacio(puerto.getIdDireccion()) 
					&& ServiciosUtil.esCampoVacio(puerto.getNroPuerto()) && ServiciosUtil.esCampoVacio(puerto.getRFS()) ){
				//notifico la ausencia del campo obligatorio
				notificarParametroObligatorio(detalleRespuesta, nombreCampo);
			}
		}

	}
	
	
	/**
	 * Lanzador de excepciones utilzado cuando fallas en la entrada de par�metros de los servicios
	 * @throws ParameterException
	 * 				Informa de la falla ocurrida
	 */
	private static void lanzarParameterException() throws ParameterException{
		Throwable t = new Throwable();
		ParameterException e = new ParameterException(t);
		throw e;
	}
	
	
	/**
	 * Funci�n que transforma la fecha de Calendario Gregoriano a formato UTCDate
	 * @param fecha
	 * 		fecha a transformar
	 * @return
	 * 		fecha en formato UTCDate
	 */
	private UTCDate obtenerFechaSolicitud(GregorianCalendar fecha) {

		UTCDate utcDate = new UTCDate();
		SimpleDateFormat format = new SimpleDateFormat(ServiciosUtil.getFormatoFechaDefault());
		BoundedString14 boundedString14 = new BoundedString14();
		boundedString14.setBoundedString14(format.format(fecha.getTime()));
		utcDate.setDate(boundedString14);
		return utcDate;

	}
	
	/**
	 * Funci�n que valida si el campo Fecha Solicitud es obligatorio. No puede ser vac�o
	 * @param nombreCampo
	 * 			Nombre del campo a validar
	 * @param fecha
	 * 			Valor del campo a validar
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void validarCampoObligatorio(String nombreCampo, UTCDate fecha,	Detallerespuestatype detalleRespuesta) throws ParameterException {
		//valido que el objeto no sea vac�o
		if (fecha==null|| fecha.getDate()==null || ServiciosUtil.esCampoVacio(fecha.getDate().getBoundedString14())){
			//notifico la ausencia del campo obligatorio
			notificarParametroObligatorio(detalleRespuesta, nombreCampo);
		}
	}
	
	/**
	 * Procedimiento que notifica la ausencia de un par�metro obligatorio en la entrada del servicio
	 * @param detalleRespuesta
	 * 			Objeto donde se retorna la respuesta
	 * @param nombreCampo
	 * 			Nombre del campo obligatorio ausente
	 * @throws ParameterException
	 * 			En caso que el campo est� vac�o se dispara una excepci�n
	 */
	private void notificarParametroObligatorio(Detallerespuestatype detalleRespuesta, String nombreCampo) throws ParameterException {
		//configuro mensaje del log
		mensajeErrorParametroInvalido = MessageFormat.format(ServiciosUtil.getMensajeErrorParametros(),keyNombreServicio,nombreCampo);
		//el campo es vacio, lanzo excepcion
		registrarException(detalleRespuesta, rb.getString(KeyRequiredParameterException), MessageFormat.format(ServiciosUtil.getMensajeErrorParametrosObligatorios(), keyNombreFuncion), null, false);
		lanzarParameterException();
		
	}

}
