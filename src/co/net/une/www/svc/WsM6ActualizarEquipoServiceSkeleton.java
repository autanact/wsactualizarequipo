
/**
 * WsM6ActualizarEquipoServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsM6ActualizarEquipoServiceSkeleton java skeleton for the axisService
     */
    public class WsM6ActualizarEquipoServiceSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsM6ActualizarEquipoServiceLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param fechaSolicitud
                                     * @param idUneEquipo
                                     * @param tipoEquipo
                                     * @param cantidadPuertos
                                     * @param puertos
                                     * @param atributos
         */
        

                 public co.net.une.www.ncainvm6.WsM6ActualizarEquipoRSType actualizarEquipo
                  (
                  co.net.une.www.ncainvm6.UTCDate fechaSolicitud,java.lang.String idUneEquipo,java.lang.String tipoEquipo,java.lang.String cantidadPuertos,co.net.une.www.ncainvm6.Listapuertotype puertos,co.net.une.www.ncainvm6.Listaatributostype atributos
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("fechaSolicitud",fechaSolicitud);params.put("idUneEquipo",idUneEquipo);params.put("tipoEquipo",tipoEquipo);params.put("cantidadPuertos",cantidadPuertos);params.put("puertos",puertos);params.put("atributos",atributos);
		try{
		
			return (co.net.une.www.ncainvm6.WsM6ActualizarEquipoRSType)
			this.makeStructuredRequest(serviceName, "actualizarEquipo", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    