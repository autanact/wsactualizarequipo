
/**
 * WsM6ActualizarEquipoServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WsM6ActualizarEquipoServiceMessageReceiverInOut message receiver
        */

        public class WsM6ActualizarEquipoServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WsM6ActualizarEquipoServiceSkeleton skel = (WsM6ActualizarEquipoServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("actualizarEquipo".equals(methodName)){
                
                co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS wsM6ActualizarEquipoRS7 = null;
	                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedParam =
                                                             (co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wsM6ActualizarEquipoRS7 =
                                                   
                                                   
                                                           wrapactualizarEquipo(
                                                       
                                                        

                                                        
                                                       skel.actualizarEquipo(
                                                            
                                                                getFechaSolicitud(wrappedParam)
                                                            ,
                                                                getIdUneEquipo(wrappedParam)
                                                            ,
                                                                getTipoEquipo(wrappedParam)
                                                            ,
                                                                getCantidadPuertos(wrappedParam)
                                                            ,
                                                                getPuertos(wrappedParam)
                                                            ,
                                                                getAtributos(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wsM6ActualizarEquipoRS7, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.ncainvm6.UTCDate getFechaSolicitud(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getFechaSolicitud();
                            
                        }
                     

                        private java.lang.String getIdUneEquipo(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getIdUneEquipo();
                            
                        }
                     

                        private java.lang.String getTipoEquipo(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getTipoEquipo();
                            
                        }
                     

                        private java.lang.String getCantidadPuertos(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getCantidadPuertos();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listapuertotype getPuertos(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getPuertos();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaatributostype getAtributos(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                        
                                return wrappedType.getWsM6ActualizarEquipoRQ().getAtributos();
                            
                        }
                     
                        private co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQType getactualizarEquipo(
                        co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ wrappedType){
                            return wrappedType.getWsM6ActualizarEquipoRQ();
                        }
                        
                        
                    
                         private co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS wrapactualizarEquipo(
                            co.net.une.www.ncainvm6.WsM6ActualizarEquipoRSType innerType){
                                co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS wrappedElement = new co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS();
                                wrappedElement.setWsM6ActualizarEquipoRS(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.WsM6ActualizarEquipoRQ.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.WsM6ActualizarEquipoRS.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    